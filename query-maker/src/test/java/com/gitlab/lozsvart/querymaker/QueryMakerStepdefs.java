package com.gitlab.lozsvart.querymaker;

import com.gitlab.lozsvart.querymaker.acceptance.Resolvers;
import io.cucumber.java.After;
import io.cucumber.java.en.*;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class QueryMakerStepdefs {

    private QueryMaker.Builder builder;
    private QueryMaker queryMaker;

    @After
    public void afterScenario() {
        queryMaker.dispose();
    }

    @Given("^I start building a query-maker instance$")
    public void step() {
        builder = QueryMaker.create();
    }

    @Given("I add a String parameter of name {string} with default value of {string}")
    public void iAddAStringParameterOfNameWithDefaultValueOf(String parameterName, String defaultValue) {
        builder.addString(parameterName, defaultValue);
    }

    @Given("I add an Integer parameter of name {string} with default value of {int}")
    public void iAddAnIntegerParameterOfNameWithDefaultValueOf(String name, int defaultValue) {
        builder.addInteger(name, defaultValue);
    }

    @Given("I add a Boolean parameter of name {string} with default value of {string}")
    public void iAddABooleanParameterOfNameWithDefaultValueOfFalse(String name, String defaultValue) {
        builder.addBoolean(name, Boolean.parseBoolean(defaultValue));
    }

    @Given("I add an enum parameter of name {string} with values of")
    public void iAddAnEnumParameterOfNameWithValuesOf(String name, List<String> values) {
        builder.addEnum(name, values);
    }

    @Given("I add a constant of name {string} with value of {string}")
    public void iAddAConstantParameterOfNameWithValueOf(String name, String value) {
        builder.addConstant(name, value);
    }

    @Given("I set auto trigger to be on")
    public void iSetAutoTriggerToBeOn() {
        builder.triggerOnStart(true);
    }

    @Given("I finish building the query-maker instance using resolver {string}")
    public void iFinishBuildingTheQueryMakerInstance(String resolver) {
        builder.resolver(Resolvers.getResolver(resolver));
        queryMaker = builder.buildDisposing();
    }

    @When("I initiate the query")
    public void iInitiateTheQuery() {
        queryMaker.initiateQuery();
    }

    @Then("the answer area should contain the value {string}")
    public void theAnswerAreaShouldContainTheValue(String expected) {
        assertThat(queryMaker.getAnswer()).isEqualTo(expected);
    }
}
