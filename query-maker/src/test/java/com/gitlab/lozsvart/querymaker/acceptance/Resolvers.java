package com.gitlab.lozsvart.querymaker.acceptance;

import com.gitlab.lozsvart.querymaker.Resolver;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Resolvers {

    private static final Map<String, Resolver> RESOLVERS = new HashMap<>();

    static {
        RESOLVERS.put("greeter", parameters -> parameters.setAnswer(parameters.get("greeting") + " " + parameters.get("name") + "!"));
        RESOLVERS.put("repeat-hello", parameters -> parameters.setAnswer(repeatHello(parameters.getInt("times"))));
        RESOLVERS.put("value-to-string", parameters -> parameters.setAnswer(parameters.get("value")));
        RESOLVERS.put("constant-value-to-string", parameters -> parameters.setAnswer(parameters.getConstant("value", String.class)));
    }

    private Resolvers() {
    }

    public static Resolver getResolver(String name) {
        return RESOLVERS.getOrDefault(name, parameters -> parameters.setAnswer("Invalid resolver"));
    }

    private static String repeatHello(int times) {
        return IntStream.range(0, times).mapToObj(i -> "hello").collect(Collectors.joining("-"));
    }

}
