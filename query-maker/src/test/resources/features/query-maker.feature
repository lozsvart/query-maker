Feature: Adding parameters

  Scenario: Adding String parameter
    Given I start building a query-maker instance
    And I add a String parameter of name "greeting" with default value of "Hello"
    And I add a String parameter of name "name" with default value of "World"
    And I finish building the query-maker instance using resolver "greeter"
    When I initiate the query
    Then the answer area should contain the value "Hello World!"

  Scenario: Adding enum parameter
    Given I start building a query-maker instance
    And I add an enum parameter of name "value" with values of
      | Hello |
      | Hi    |
    And I finish building the query-maker instance using resolver "value-to-string"
    When I initiate the query
    Then the answer area should contain the value "Hello"

  Scenario: Adding int parameter
    Given I start building a query-maker instance
    And I add an Integer parameter of name "times" with default value of 3
    And I finish building the query-maker instance using resolver "repeat-hello"
    When I initiate the query
    Then the answer area should contain the value "hello-hello-hello"

  Scenario: Adding boolean parameter
    Given I start building a query-maker instance
    And I add a Boolean parameter of name "value" with default value of "true"
    And I finish building the query-maker instance using resolver "value-to-string"
    When I initiate the query
    Then the answer area should contain the value "true"

  Scenario: Adding a constant parameter
    Given I start building a query-maker instance
    And I add a constant of name "value" with value of "abcde"
    And I finish building the query-maker instance using resolver "constant-value-to-string"
    When I initiate the query
    Then the answer area should contain the value "abcde"

  Scenario: Auto trigger on startup
    Given I start building a query-maker instance
    And I add a constant of name "value" with value of "abcde"
    And I set auto trigger to be on
    When I finish building the query-maker instance using resolver "constant-value-to-string"
    Then the answer area should contain the value "abcde"
