package com.gitlab.lozsvart.querymaker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.*;

/**
 * Class for creating simple GUIs to execute queries.
 */
public class QueryMaker {

    private static final String ANSWER_COLUMN = "Answer";
    private static final String TIMESTAMP_COLUMN = "Timestamp";
    private final Map<String, Supplier<String>> parameterGetterByName = new HashMap<>();
    private final Map<String, Consumer<String>> parameterSetterByName = new HashMap<>();
    private final Map<String, Object> constants;
    private final List<String> parameterList;
    private Consumer<String> answerSetter;
    private Supplier<String> answerGetter;
    private JButton defaultButton;
    private JComponent inputComponent;
    private JComponent queryInitiatingComponent;
    private JComponent outputComponent;

    private final Resolver resolver;
    private JFrame frame;

    private final boolean recordHistory;
    private boolean historyShown = false;
    private JFrame history;
    private JTable historyTable;
    private JButton historyButton;

    private QueryMaker(Builder builder) {
        validateParameters(builder.parameters);
        this.resolver = builder.resolver;
        this.constants = builder.constants;
        this.parameterList = builder.parameters.stream().map(parameter -> parameter.name).collect(Collectors.toList());
        this.recordHistory = builder.recordHistory;

        createInputComponent(builder.parameters, builder.preferredParameterColumns);
        createOutputComponent(builder);
        createQueryInitiatingComponent();

        createFrame(builder);

        if (builder.triggerOnStart) {
            defaultButton.doClick();
        }

    }

    private void validateParameters(List<Parameter> parameters) {
        int uniqueParameters = parameters.stream()
                .map(parameter -> parameter.name)
                .collect(Collectors.toSet())
                .size();
        if (uniqueParameters < parameters.size()) {
            throw new IllegalArgumentException("Parameters must be unique");
        }
    }

    private void createFrame(Builder builder) {
        String postfix = builder.name == null ? "" : " - " + builder.name;
        frame = new JFrame("QueryMaker" + postfix);
        frame.add(createContentPanel());
        frame.pack();
        frame.getRootPane().setDefaultButton(defaultButton);
        frame.setDefaultCloseOperation(builder.closeOperation);
        frame.setLocationRelativeTo(null);
        if (recordHistory) {
            createHistoryFrame(postfix);
        }
        frame.setVisible(true);
    }

    private void createHistoryFrame(String postfix) {
        history = new JFrame("History" + postfix);
        historyTable = new JTable(new DefaultTableModel(new Object[][]{}, getTableHeaders()));
        historyTable.setPreferredScrollableViewportSize(new Dimension(500, 500));
        historyTable.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(historyTable);
        historyTable.setFillsViewportHeight(true);
        history.add(scrollPane);
        history.setLocationRelativeTo(frame);
        history.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        history.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                toggleHistory();
            }
        });
        history.pack();
        updateHistoryShown();
    }

    private void addToHistory(HistoryItem historyItem) {
        Parameters parameters = historyItem.parameters;
        DefaultTableModel model = (DefaultTableModel) historyTable.getModel();
        String[] tableHeaders = getTableHeaders();
        Object[] row = Stream.concat(IntStream.range(0, parameterList.size())
                        .mapToObj(i -> parameters.get(tableHeaders[i])), Stream.of(historyItem.answer, formatTimestamp(historyItem)))
                .toArray();
        model.addRow(row);
    }

    private String formatTimestamp(HistoryItem historyItem) {
        return historyItem.timestamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    private String[] getTableHeaders() {
        List<String> tableHeaders = new LinkedList<>(parameterList);
        tableHeaders.add(ANSWER_COLUMN);
        tableHeaders.add(TIMESTAMP_COLUMN);
        return tableHeaders.toArray(new String[]{});
    }

    private void createInputComponent(List<Parameter> parameters, int preferredColumns) {
        int columns = Math.max(1, Math.min(preferredColumns, parameters.size()));
        JPanel parametersPanel = new JPanel(new GridLayout(0, columns, 2, 2));
        for (Parameter parameter : parameters) {
            parameterGetterByName.put(parameter.name, parameter.getter);
            parameterSetterByName.put(parameter.name, parameter.setter);
            parametersPanel.add(createParameterPanel(parameter, parameter.component));
        }
        this.inputComponent = parametersPanel;
    }

    private JPanel createContentPanel() {
        JPanel northPanel = new JPanel();
        northPanel.add(inputComponent);
        northPanel.add(queryInitiatingComponent);
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.add(northPanel, BorderLayout.NORTH);
        contentPanel.add(outputComponent, BorderLayout.CENTER);
        return contentPanel;
    }

    private JPanel createParameterPanel(Parameter parameter, Component parameterComponent) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(parameter.name + ":"));
        panel.add(parameterComponent);
        return panel;
    }

    private void createQueryInitiatingComponent() {
        JButton okButton = new JButton("OK");
        JPanel queryInitiatingPanel = new JPanel();
        queryInitiatingPanel.add(okButton);
        okButton.addActionListener(action -> initiateQuery());
        if (recordHistory) {
            historyButton = new JButton(getHistoryButtonLabel());
            historyButton.addActionListener(action -> toggleHistory());
            queryInitiatingPanel.add(historyButton);
        }
        this.queryInitiatingComponent = queryInitiatingPanel;
        this.defaultButton = okButton;
    }

    private void toggleHistory() {
        historyShown = !historyShown;
        updateHistoryShown();
    }

    private void updateHistoryShown() {
        historyButton.setText(getHistoryButtonLabel());
        history.setVisible(historyShown);
    }

    private String getHistoryButtonLabel() {
        return historyShown ? "Hide history" : "Show history";
    }

    void initiateQuery() {
        Parameters params = getParams();
        Parameters snapshot = snapshot(params);
        ZonedDateTime timestamp = ZonedDateTime.now();
        resolver.resolve(params);
        if (recordHistory) {
            HistoryItem historyItem = new HistoryItem();
            historyItem.parameters = snapshot;
            historyItem.answer = params.getAnswer();
            historyItem.timestamp = timestamp;
            addToHistory(historyItem);
        }
    }

    private Parameters snapshot(Parameters params) {
        return new Parameters() {
            private String answer;
            private final Map<String, String> parameterValueByName = new HashMap<>();

            {
                parameterList.forEach(parameter -> parameterValueByName.put(parameter, params.get(parameter)));
            }

            @Override
            public String getAnswer() {
                return answer;
            }

            @Override
            public void setAnswer(String value) {
                this.answer = value;
            }

            @Override
            public String get(String parameter) {
                return parameterValueByName.get(parameter);
            }

            @Override
            public void set(String parameter, String value) {
                throw new IllegalStateException("Set answer should not be called on snapshot parameter");
            }

            @Override
            public <T> T getConstant(String name, Class<T> type) {
                return params.getConstant(name, type);
            }
        };
    }

    String getAnswer() {
        return answerGetter.get();
    }

    void dispose() {
        frame.dispose();
    }

    private Parameters getParams() {
        return new Parameters() {

            @Override
            public String getAnswer() {
                return answerGetter.get();
            }

            @Override
            public void setAnswer(String value) {
                answerSetter.accept(value);
            }

            @Override
            public String get(String parameter) {
                validateParameterExists(parameter);
                return parameterGetterByName.get(parameter).get();
            }

            @Override
            public void set(String parameter, String value) {
                validateParameterExists(parameter);
                parameterSetterByName.get(parameter).accept(value);
            }

            @Override
            public <T> T getConstant(String name, Class<T> type) {
                if (!constants.containsKey(name)) {
                    throw new IllegalArgumentException("Parameter does not exist");
                }
                return type.cast(constants.get(name));
            }

            private void validateParameterExists(String parameter) {
                if (!parameterGetterByName.containsKey(parameter)) {
                    throw new IllegalArgumentException("Parameter does not exist");
                }
            }
        };
    }

    private void createOutputComponent(Builder builder) {
        JTextArea answerArea = new JTextArea();
        answerArea.setFont(new Font("Courier New", Font.PLAIN, 12));
        answerArea.setColumns(builder.answerColumns);
        answerArea.setRows(builder.answerRows);
        answerArea.setLineWrap(true);
        answerArea.setEditable(builder.answerEditable);
        this.answerSetter = answerArea::setText;
        this.answerGetter = answerArea::getText;
        this.outputComponent = new JScrollPane(answerArea);
    }

    /**
     * Returns a new Builder that can be used to create a new instance of QueryMaker.
     *
     * @return the builder
     */
    public static Builder create() {
        return new Builder();
    }

    /**
     * Used for creating a new instance of QueryMaker.
     */
    public static class Builder {

        private String name;
        private final List<Parameter> parameters = new LinkedList<>();
        private final Map<String, Object> constants = new HashMap<>();
        private int closeOperation;
        private Resolver resolver = params -> {
        };
        private int answerColumns = 50;
        private int answerRows = 10;
        private int preferredParameterColumns = 3;
        private boolean answerEditable = false;
        private boolean triggerOnStart = false;
        private boolean recordHistory = false;

        private Builder() {
        }

        /**
         * Sets the name of the displayed window.
         *
         * @param name the name to set
         * @return self
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets whether the answer area is editable from the UI.
         *
         * @param answerEditable whether the answer area should be editable from the UI
         * @return self
         */
        public Builder answerEditable(boolean answerEditable) {
            this.answerEditable = answerEditable;
            return this;
        }

        /**
         * Sets the dimensions of the answer area.
         *
         * @param columns number of columns to display
         * @param rows    number of rows to display
         * @return self
         */
        public Builder answerSize(int columns, int rows) {
            this.answerColumns = columns;
            this.answerRows = rows;
            return this;
        }

        /**
         * Sets the preferred number of parameters to be displayed in the input area in each row.
         *
         * @param preferredParameterColumns number of parameters per row
         * @return self
         */
        public Builder preferredParameterColumns(int preferredParameterColumns) {
            if (preferredParameterColumns < 1) {
                throw new IllegalArgumentException("Must be at least 1");
            }
            this.preferredParameterColumns = preferredParameterColumns;
            return this;
        }

        /**
         * Adds a new parameter to the GUI of type String.
         *
         * @param parameterName name of the parameter
         * @param defaultValue  the value to initialize the field with
         * @param size          number of columns to display
         * @return self
         */
        public Builder addString(String parameterName, String defaultValue, int size) {
            Parameter parameter = new Parameter();
            JTextField textField = new JTextField();
            textField.addFocusListener(selectingFocusListener(textField));
            textField.setColumns(size);
            textField.setText(defaultValue);

            parameter.component = textField;
            parameter.getter = textField::getText;
            parameter.setter = textField::setText;
            parameter.name = parameterName;
            parameters.add(parameter);
            return this;
        }

        /**
         * Adds a new parameter to the GUI of type String.
         *
         * @param parameterName name of the parameter
         * @param defaultValue  the value to initialize the field with
         * @return self
         */
        public Builder addString(String parameterName, String defaultValue) {
            return addString(parameterName, defaultValue, 10);
        }

        /**
         * Adds a new parameter to the GUI of type String.
         *
         * @param parameterName name of the parameter
         * @return self
         */
        public Builder addString(String parameterName) {
            return addString(parameterName, "", 10);
        }

        /**
         * Adds a new parameter to the GUI of type boolean.
         *
         * @param parameterName name of the parameter
         * @param defaultValue  the value to initialize the field with
         * @return self
         */
        public Builder addBoolean(String parameterName, boolean defaultValue) {
            Parameter parameter = new Parameter();
            JCheckBox checkBox = new JCheckBox("", defaultValue);
            parameter.name = parameterName;
            parameter.component = checkBox;
            parameter.getter = () -> Boolean.toString(checkBox.isSelected());
            parameter.setter = value -> checkBox.setSelected(Boolean.parseBoolean(value));
            parameters.add(parameter);
            return this;
        }

        /**
         * Adds a new parameter to the GUI of type boolean, with default value of false.
         *
         * @param parameterName name of the parameter
         * @return self
         */
        public Builder addBoolean(String parameterName) {
            return addBoolean(parameterName, false);
        }

        /**
         * Adds a new parameter to the GUI of type String, selectable from a dropdown menu.
         *
         * @param parameterName name of the parameter
         * @param values        list of selectable values
         * @return self
         */
        public Builder addEnum(String parameterName, List<String> values) {
            Parameter parameter = new Parameter();
            parameter.name = parameterName;
            Choice choice = new Choice();
            values.forEach(choice::add);
            parameter.component = choice;
            parameter.getter = choice::getSelectedItem;
            parameter.setter = choice::select;
            parameters.add(parameter);
            return this;
        }

        /**
         * Adds a new parameter to the GUI of type integer.
         *
         * @param parameterName name of the parameter
         * @param defaultValue  the value to initialize the field with
         * @param size          number of columns to display
         * @return self
         */
        public Builder addInteger(String parameterName, int defaultValue, int size) {
            Parameter parameter = new Parameter();
            parameter.name = parameterName;
            JSpinner jSpinner = new JSpinner();
            jSpinner.setValue(defaultValue);
            ((JSpinner.DefaultEditor) jSpinner.getEditor()).getTextField().setColumns(size);
            parameter.component = jSpinner;
            parameter.getter = () -> jSpinner.getValue().toString();
            parameter.setter = value -> jSpinner.setValue(Integer.parseInt(value));
            parameters.add(parameter);
            return this;
        }

        /**
         * Adds a new parameter to the GUI of type integer.
         *
         * @param parameterName name of the parameter
         * @param defaultValue  the value to initialize the field with
         * @return self
         */
        public Builder addInteger(String parameterName, int defaultValue) {
            return addInteger(parameterName, defaultValue, 5);
        }

        /**
         * Adds a new constant. Constants have no representation on the UI, but they can be queried by the Resolver.
         *
         * @param name     name of the constant
         * @param constant value
         * @return self
         */
        public Builder addConstant(String name, Object constant) {
            constants.put(name, constant);
            return this;
        }

        /**
         * Sets the Resolver that will be called when the OK button is pressed.
         *
         * @param resolver the resolver
         * @return self
         */
        public Builder resolver(Resolver resolver) {
            this.resolver = Objects.requireNonNull(resolver);
            return this;
        }

        /**
         * Sets whether the OK button should be pressed when the application starts up.
         *
         * @param triggerOnStart if the OK button should be pressed on startup
         * @return self
         */
        public Builder triggerOnStart(boolean triggerOnStart) {
            this.triggerOnStart = triggerOnStart;
            return this;
        }

        /**
         * Sets whether OK button presses should be recorded
         *
         * @param recordHistory if the history of input parameters should be recorded
         * @return self
         */
        public Builder recordHistory(boolean recordHistory) {
            this.recordHistory = recordHistory;
            return this;
        }

        /**
         * Builds the GUI. Closing the windows will exit the program.
         *
         * @return self
         */
        public QueryMaker buildExiting() {
            closeOperation = WindowConstants.EXIT_ON_CLOSE;
            return new QueryMaker(this);
        }

        /**
         * Builds the GUI. Closing the windows will let the program continue running.
         *
         * @return self
         */
        public QueryMaker buildDisposing() {
            closeOperation = WindowConstants.DISPOSE_ON_CLOSE;
            return new QueryMaker(this);
        }


    }

    private static class Parameter {

        private String name;
        private Component component;
        private Supplier<String> getter;
        private Consumer<String> setter;

    }

    private static FocusListener selectingFocusListener(JTextComponent textComponent) {
        return new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                textComponent.select(0, textComponent.getText().length());
            }

            @Override
            public void focusLost(FocusEvent e) {
                textComponent.select(0, 0);
            }
        };
    }

    private static class HistoryItem {

        Parameters parameters;
        String answer;
        ZonedDateTime timestamp;

    }
}
