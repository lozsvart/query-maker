## Description
This is a lightweight component to quickly create GUIs for making queries.

## Prerequisites
* Java 8

## Usage
See the tests
