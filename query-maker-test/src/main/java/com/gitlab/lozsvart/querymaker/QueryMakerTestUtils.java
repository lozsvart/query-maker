package com.gitlab.lozsvart.querymaker;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Utility class used for converting QueryMaker instances into automated tests.
 */
public class QueryMakerTestUtils {

    private QueryMakerTestUtils() {
    }

    /**
     * Executes a test based on the arguments.
     *
     * @param resolver     the resolver to execute
     * @param parameters   the parameters to pass
     * @param verification the verification to execute on the parameters after the resolver is executed
     */
    public static void executeTest(Resolver resolver, Map<String, String> parameters, Consumer<Parameters> verification) {
        Parameters params = toParameters(parameters);
        resolver.resolve(params);
        verification.accept(params);
    }

    /**
     * Executes a test based on the arguments.
     *
     * @param resolver     the resolver to execute
     * @param parameters   the parameters to pass
     * @param constants    the constants to pass
     * @param verification the verification to execute on the parameters after the resolver is executed
     */
    public static void executeTest(Resolver resolver, Map<String, String> parameters, Map<String, Object> constants, Consumer<Parameters> verification) {
        Parameters params = toParameters(parameters, constants);
        resolver.resolve(params);
        verification.accept(params);
    }

    /**
     * Returns a verifier to check if the answer equals to a certain value.
     *
     * @param value the expected value
     * @return the verification
     */
    public static Consumer<Parameters> answerEquals(String value) {
        return parameters -> assertThat(parameters.getAnswer()).isEqualTo(value);
    }

    private static Parameters toParameters(Map<String, String> parameters) {
        return new TestParameters(parameters);
    }

    private static Parameters toParameters(Map<String, String> parameters, Map<String, Object> constants) {
        return new TestParameters(parameters, constants);
    }

    private static class TestParameters implements Parameters {

        private final Map<String, String> parameters;
        private final Map<String, Object> constants;
        private String answer;

        public TestParameters(Map<String, String> parameters) {
            this.parameters = parameters;
            this.constants = new HashMap<>();
        }

        public TestParameters(Map<String, String> parameters, Map<String, Object> constants) {
            this.parameters = parameters;
            this.constants = constants;
        }

        @Override
        public String getAnswer() {
            return answer;
        }

        @Override
        public void setAnswer(String value) {
            this.answer = answer;
        }

        @Override
        public String get(String parameter) {
            return parameters.get(parameter);
        }

        @Override
        public void set(String parameter, String value) {
            parameters.put(parameter, value);
        }

        @Override
        public <T> T getConstant(String name, Class<T> type) {
            return type.cast(constants.get(name));
        }
    }


}
