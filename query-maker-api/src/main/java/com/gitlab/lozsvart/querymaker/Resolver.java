package com.gitlab.lozsvart.querymaker;

/**
 * A functional interface that can be implemented to execute queries.
 */
@FunctionalInterface
public interface Resolver {

    /**
     * A query to be executed when the OK button is pressed-
     *
     * @param parameters the parameters and constants available on the UI
     */
    void resolve(Parameters parameters);

}
