package com.gitlab.lozsvart.querymaker;

/**
 * Interface to expose interaction with the UI.
 */
public interface Parameters {

    /**
     * Allows reading the content of the answer area.
     *
     * @return the content
     */
    String getAnswer();

    /**
     * Allows writing to the answer area.
     *
     * @param value the value to write
     */
    void setAnswer(String value);

    /**
     * Allows reading a parameter of type String.
     *
     * @param parameter parameter name
     * @return parameter value
     */
    String get(String parameter);

    /**
     * Allows writing a parameter of type String.
     *
     * @param parameter parameter name
     * @param value     parameter value
     */
    void set(String parameter, String value);

    /**
     * Allows reading a constant of the provided type. Parameters are not constants and vice versa.
     *
     * @param name constant name
     * @param type the expected type
     * @param <T>  the expected type
     * @return constant value
     */
    <T> T getConstant(String name, Class<T> type);

    /**
     * Allows reading a parameter of type boolean.
     *
     * @param parameter parameter name
     * @return parameter value
     */
    default boolean getBoolean(String parameter) {
        return Boolean.parseBoolean(get(parameter));
    }

    /**
     * Allows writing a parameter of type boolean.
     *
     * @param parameter parameter name
     * @param value     parameter value
     */
    default void setBoolean(String parameter, boolean value) {
        set(parameter, Boolean.toString(value));
    }

    /**
     * Allows reading a parameter of type int.
     *
     * @param parameter parameter name
     * @return parameter value
     */
    default int getInt(String parameter) {
        return Integer.parseInt(get(parameter));
    }

    /**
     * Allows writing a parameter of type int.
     *
     * @param parameter parameter name
     * @param value     parameter value
     */
    default void setInt(String parameter, int value) {
        set(parameter, Integer.toString(value));
    }

}
